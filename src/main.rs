// SPDX-FileCopyrightText: 2024 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use ipp::attribute::{IppAttribute, IppAttributeGroup, IppAttributes};
use ipp::model::IppVersion;
use ipp::model::{DelimiterTag, JobState, Operation, StatusCode};
use ipp::prelude::Uri;
use ipp::{parser::AsyncIppParser, request::IppRequestResponse, value::IppValue};

use rocket::{
    Request, State, catch, catchers,
    data::{Data, ToByteUnit},
    http::uri::{Authority, Host},
    launch, post,
    response::Responder,
    routes,
};

use tokio::{fs::File, io::AsyncRead, sync::Mutex};
use tokio_util::compat::TokioAsyncReadCompatExt;

use log::{error, info, trace};

use num_traits::{FromPrimitive, ToPrimitive};
use std::{collections::HashMap, path::PathBuf, process, sync::Arc};

#[derive(Debug)]
struct Job {
    state: JobState,
}

struct PrinterState {
    jobs: HashMap<i32, Job>,
    last_id: i32,
}

impl PrinterState {
    fn get_id(&mut self) -> i32 {
        self.last_id += 1;
        self.last_id
    }
}

struct Printer {
    file: String,
}

async fn print<T: AsyncRead + Unpin>(document: &mut T, path: &str) -> anyhow::Result<()> {
    let mut file = File::create(path).await?;
    tokio::io::copy(document, &mut file).await?;

    Ok(())
}

#[derive(Responder)]
#[response(status = 200, content_type = "application/ipp")]
struct IppData {
    data: Vec<u8>,
}

/// Get the job object relating to the current request
/// First, the job-id will be used, but if it was not sent,
/// the function will fall back to job-uri.
async fn get_requested_job(
    state: &mut PrinterState,
    attributes: IppAttributes,
) -> Option<(i32, &mut Job)> {
    let attrs: HashMap<&str, &IppAttribute> = attributes
        .groups_of(DelimiterTag::OperationAttributes)
        .flat_map(|g| g.attributes())
        .map(|(k, a)| (k.as_str(), a))
        .collect();

    let job = match attrs.get(IppAttribute::JOB_ID) {
        Some(attr) => match attr.value().as_integer() {
            Some(id) => match state.jobs.get_mut(id) {
                Some(job) => Some((*id, job)),
                None => {
                    error!("Requested job does not exist: {id}");
                    None
                }
            },
            None => {
                error!("Invalid job id");
                None
            }
        },
        None => {
            trace!("No job-id, falling back to job-uri parsing");
            let uri = attrs.get(IppAttribute::JOB_URI);
            match uri {
                Some(attr) => {
                    let uri = attr.value().as_uri();
                    match uri {
                        Some(uri) => match Uri::from_maybe_shared(uri.clone()) {
                            Ok(uri) => {
                                let job_id = uri
                                    .path_and_query()
                                    .and_then(|pq| pq.path().split('/').find(|s| !s.is_empty()))
                                    .and_then(|id| id.parse().ok());

                                trace!("job-id from job-uri: {:?}", job_id);

                                match job_id {
                                    Some(job_id) => match state.jobs.get_mut(&job_id) {
                                        Some(job) => Some((job_id, job)),
                                        None => {
                                            error!("Requested job does not exist: {job_id}");
                                            None
                                        }
                                    },
                                    None => {
                                        error!("Failed to extract job-id from uri {uri}");
                                        None
                                    }
                                }
                            }
                            Err(e) => {
                                error!("Failed to parse uri {uri} because {e}");
                                None
                            }
                        },
                        None => {
                            error!("invalid job-uri");
                            None
                        }
                    }
                }
                None => {
                    error!("neither job-id nor job-uri");
                    None
                }
            }
        }
    };

    job
}

fn make_job_uri(domain: &str, port: Option<u16>, id: i32) -> Uri {
    let authority = match port {
        Some(port) => {
            format!("{domain}:{port}")
        }
        None => domain.to_string(),
    };
    Uri::builder()
        .scheme("ipp")
        .authority(authority)
        .path_and_query(format!("/{id}"))
        .build()
        .unwrap()
}

#[test]
fn test_job_uri() {
    let uri = make_job_uri("localhost", Some(631), 1);
    assert_eq!(uri.to_string(), "ipp://localhost:631/1")
}

#[post("/", data = "<body>")]
async fn ipp_endpoint(
    host: &Host<'_>,
    body: Data<'_>,
    state: &State<Arc<Mutex<PrinterState>>>,
    printer: &State<Arc<Mutex<Printer>>>,
) -> IppData {
    let stream = body.open(4.gibibytes());
    let parser = AsyncIppParser::new(stream.compat());

    let (header, attributes, reader) = match parser.parse_parts().await {
        Ok(parsed) => parsed,
        Err(e) => {
            error!("Couldn't parse IPP stream: {e}");

            let response = IppRequestResponse::new_response(
                IppVersion::v1_1(),
                StatusCode::ClientErrorBadRequest,
                0,
            );
            return IppData {
                data: response.to_bytes().to_vec(),
            };
        }
    };

    let mut response = IppRequestResponse::new_response(
        IppVersion::v1_1(),
        StatusCode::SuccessfulOk,
        header.request_id,
    );
    let resp_attributes = response.attributes_mut();

    let operation = Operation::from_u16(header.operation_or_status).unwrap();
    info!("   >> Operation: {:?}", operation);
    match operation {
        Operation::GetPrinterAttributes => {
            resp_attributes.add(
                DelimiterTag::PrinterAttributes,
                IppAttribute::new("attributes-charset", "utf-8".parse().unwrap()),
            );
            resp_attributes.add(
                DelimiterTag::PrinterAttributes,
                IppAttribute::new("attributes-natural-language", "en-us".parse().unwrap()),
            );
            for group in attributes.groups() {
                for attribute in group.attributes().values() {
                    resp_attributes.add(DelimiterTag::UnsupportedAttributes, attribute.clone())
                }
            }
        }
        Operation::PrintJob => {
            let id = state.lock().await.get_id();
            state.lock().await.jobs.insert(
                id,
                Job {
                    state: JobState::Pending,
                },
            );

            resp_attributes.add(
                DelimiterTag::JobAttributes,
                IppAttribute::new(IppAttribute::JOB_ID, IppValue::Integer(id as i32)),
            );

            let uri = make_job_uri(host.domain().as_str(), host.port(), id);
            resp_attributes.add(
                DelimiterTag::JobAttributes,
                IppAttribute::new(IppAttribute::JOB_URI, IppValue::Uri(uri.to_string())),
            );
            resp_attributes.add(
                DelimiterTag::JobAttributes,
                IppAttribute::new(
                    IppAttribute::JOB_STATE_REASONS,
                    IppValue::Keyword("none".to_string()),
                ),
            );

            let path = &printer.lock().await.file;

            state
                .lock()
                .await
                .jobs
                .get_mut(&id)
                .expect("We just added it")
                .state = JobState::Processing;

            match print(reader.into_inner().get_mut(), path).await {
                Err(e) => {
                    let mut state = state.lock().await;
                    let job = state.jobs.get_mut(&id).expect("We just added this");
                    job.state = JobState::Aborted;
                    resp_attributes.add(
                        DelimiterTag::JobAttributes,
                        IppAttribute::new(
                            IppAttribute::JOB_STATE,
                            IppValue::Enum(job.state.to_i32().expect("max number is 9")),
                        ),
                    );

                    error!("Could not send data to printer: {e}");
                    response.header_mut().operation_or_status =
                        StatusCode::ServerErrorDeviceError as u16;
                }
                _ => {
                    let mut state = state.lock().await;
                    let job = state.jobs.get_mut(&id).expect("We just added this");
                    job.state = JobState::Completed;
                    resp_attributes.add(
                        DelimiterTag::JobAttributes,
                        IppAttribute::new(
                            IppAttribute::JOB_STATE,
                            IppValue::Enum(job.state.to_i32().expect("max number is 9")),
                        ),
                    );
                }
            }
        }
        Operation::CancelJob => {
            let mut state = state.lock().await;
            let job = get_requested_job(&mut state, attributes).await;
            match job {
                Some((_id, job)) => {
                    let status = match job.state {
                        JobState::Pending | JobState::PendingHeld => StatusCode::SuccessfulOk,
                        JobState::Processing
                        | JobState::ProcessingStopped
                        | JobState::Completed
                        | JobState::Canceled
                        | JobState::Aborted => StatusCode::ClientErrorNotPossible,
                    };
                    job.state = match job.state {
                        JobState::Pending => JobState::Canceled,
                        JobState::PendingHeld => JobState::Canceled,
                        JobState::Processing => JobState::Processing,
                        JobState::ProcessingStopped => JobState::ProcessingStopped,
                        JobState::Completed => JobState::Completed,
                        JobState::Canceled => JobState::Canceled,
                        JobState::Aborted => JobState::Aborted,
                    };
                    response.header_mut().operation_or_status = status as u16;
                }
                None => {
                    response.header_mut().operation_or_status =
                        StatusCode::ClientErrorBadRequest as u16;
                }
            }
        }
        Operation::GetJobAttributes => {
            let mut state = state.lock().await;
            let job = get_requested_job(&mut state, attributes).await;

            match job {
                Some((id, job)) => {
                    resp_attributes.add(
                        DelimiterTag::JobAttributes,
                        IppAttribute::new(IppAttribute::JOB_ID, IppValue::Integer(id)),
                    );
                    resp_attributes.add(
                        DelimiterTag::JobAttributes,
                        IppAttribute::new(
                            IppAttribute::JOB_STATE,
                            IppValue::Enum(job.state.to_i32().unwrap()),
                        ),
                    );
                    let uri = make_job_uri(host.domain().as_str(), host.port(), id);
                    resp_attributes.add(
                        DelimiterTag::JobAttributes,
                        IppAttribute::new(IppAttribute::JOB_URI, IppValue::Uri(uri.to_string())),
                    );
                }
                None => {
                    response.header_mut().operation_or_status =
                        StatusCode::ClientErrorNotFound as u16;
                }
            }
        }
        Operation::GetJobs => {
            let jobs = &state.lock().await.jobs;
            for (id, job) in jobs {
                let mut group = IppAttributeGroup::new(DelimiterTag::JobAttributes);
                group.attributes_mut().insert(
                    IppAttribute::JOB_ID.to_owned(),
                    IppAttribute::new(IppAttribute::JOB_ID, IppValue::Integer(*id)),
                );
                group.attributes_mut().insert(
                    IppAttribute::JOB_STATE.to_owned(),
                    IppAttribute::new(
                        IppAttribute::JOB_STATE,
                        IppValue::Enum(job.state.to_i32().unwrap()),
                    ),
                );
                let uri = make_job_uri(host.domain().as_str(), host.port(), *id);
                group.attributes_mut().insert(
                    IppAttribute::JOB_URI.to_owned(),
                    IppAttribute::new(IppAttribute::JOB_URI, IppValue::Uri(uri.to_string())),
                );
                resp_attributes.groups_mut().push(group);
            }
        }
        _ => {
            response.header_mut().operation_or_status =
                StatusCode::ServerErrorOperationNotSupported as u16;
        }
    }

    IppData {
        data: response.to_bytes().to_vec(),
    }
}

#[derive(Responder)]
#[response(status = 404, content_type = "text/html")]
struct NotFound {
    html: String,
}

#[catch(404)]
fn not_found(req: &Request<'_>) -> NotFound {
    let config = req.rocket().config();
    let fallback_host =
        Host::new(Authority::parse_owned(format!("{}:{}", config.address, config.port)).unwrap());
    let host = req.host().unwrap_or(&fallback_host);
    let domain = host.domain();
    let port = host.port().unwrap_or(config.port);
    let html = format!(
        r#"
<center>
<h2>This is an IPP print server.</h2>
<p>Use it by pointing an IPP client (e.g. CUPS) to <code>ipp://{}:{}/</code></p>
</center>
<hr>
<center>
<a href="https://gitlab.spline.de/spline/mateprint/rawqueued">rawqueued/{}</a>
</center>
"#,
        domain,
        port,
        env!("VERGEN_GIT_DESCRIBE")
    );

    NotFound { html }
}

fn detect_printer() -> Option<String> {
    for i in 0..100 {
        let path = format!("/dev/usb/lp{i}");
        if PathBuf::from(&path).exists() {
            return Some(path);
        }
    }

    None
}

#[launch]
fn rocket() -> _ {
    let file = match detect_printer() {
        Some(path) => path,
        None => {
            eprintln!("Could not find printer in /dev/usb/lp*");
            process::exit(1)
        }
    };

    rocket::build()
        .manage(Arc::new(Mutex::new(PrinterState {
            jobs: HashMap::new(),
            last_id: 0,
        })))
        .manage(Arc::new(Mutex::new(Printer { file })))
        .mount("/", routes![ipp_endpoint])
        .register("/", catchers![not_found])
}
