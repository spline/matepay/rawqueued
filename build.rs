use vergen::Emitter;
use vergen_gitcl::GitclBuilder;

pub fn main() -> anyhow::Result<()> {
    Emitter::default()
        .add_instructions(&GitclBuilder::all_git()?)?
        .emit()
}
